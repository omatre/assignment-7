﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ZoologyFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            var animals = new Dictionary<int, Animal>();

            var animal1 = new Carnivore("South Africa", 8);
            var animal2 = new Herbivore("Spain", 15);
            var animal3 = new Herbivore("Benin", 12);
            var animal4 = new Carnivore("Downtown Oslo", 120);

            animals.Add(animal1.Tag, animal1);
            animals.Add(animal2.Tag, animal2);
            animals.Add(animal3.Tag, animal3);
            animals.Add(animal4.Tag, animal4);

            foreach (var animal in animals)
            {
                Animal a = animal.Value;

                Console.WriteLine($"It's a { a.GetType().Name } of age { a.Age } with the tagnumber { a.Tag } from { a.Location }.");
                a.Breathing();
                a.Eat();
            }
        }
    }

    class Herbivore : Animal
    {
        public Herbivore(string location, int age) : base(location, age)
        {
        }

        public override void Eat()
        {
            Console.WriteLine($"Animal { Tag } eats plants and minds its own buisness.");
        }
    }

    class Carnivore : Animal
    {
        public Carnivore(string location, int age) : base(location, age)
        {
        }

        public override void Eat()
        {
            Console.WriteLine($"Animal { Tag } eats a defenceless animal.");
        }
    }

    abstract class Animal
    {
        protected static List<int> TagRegister;

        public string Location { get; set; }
        public int Age { get; set; }
        public int Tag { get; private set; }

        public Animal(string location, int age)
        {
            if (TagRegister == null)
            {
                TagRegister = new List<int>();
            }

            Location = location;
            Age = age;
            Tag = GenerateTag();
        }

        private int GenerateTag()
        {
            Random random = new Random();
            int num = random.Next(1000, 10000);

            if (TagRegister.Contains(num))
            {
                return GenerateTag();
            }
            else
            {
                TagRegister.Add(num);
                return num;
            }
        }

        public virtual void Breathing()
        {
            Console.WriteLine($"Animal { Tag } is breathing. Oh, happy days.");
        }

        public abstract void Eat();
    }
}
